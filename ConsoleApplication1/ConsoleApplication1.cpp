﻿#include <iostream>
#include <ctime>

int main()
{
    const int N = 4;
    int array[N][N];

    time_t t = time(0);
    struct tm timeinfo;
    localtime_s(&timeinfo, &t);
    int day = timeinfo.tm_mday;

    int row = day % N;
    int sum = 0;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j];
            if (i == row)
            {
                sum += array[i][j];
            }
        }
        std::cout << "\n";
    }
    std::cout << sum;


 


}